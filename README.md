## PersistentCookieJar

## Introduction

A persistent CookieJar implementation for OkHttp3 based on SharedPreferences. This library is normally used to store cookies got from the http url. Again if we hit the url & get the cookies so before saving it will check the cookie is expired or not. If its expired then it will clear previous & save new one.

### Features:

This is a really simple library but here are some of the things that it provides:

#### Preferences:

Note:
*  Possibility to clear the jar: **PersistentCookieJar** implements **ClearableCookieJar** interface that declares a **clear()** method for removing all cookies from the jar.

*  Possibility to clear session cookies: **PersistentCookieJar** implements **ClearableCookieJa** interface that declares a **clearSession()** method for removing session cookies from the jar.

*  Decoupled and extensible: **CookieCache** and **CookiePersistor** are interfaces so you can provide your own implementation for each one.
        
*  **CookieCache** represents an in-memory cookie storage. **SetCookieCache** is the provided implementation that uses a Set to store the Cookies.

*  **CookiePersistor** represents a persistent storage. **SharedPrefsCookiePersistor** is the provided implementation that uses a SharedPreferences to persist the Cookies.

*  Thread-safe: **PersistentCookieJar** public methods are synchronized so there is no need to worry about threading if you need to implement a **CookieCache** or a **CookiePersistor**.

## Usage instructions

- Create an instance of **PersistentCookieJar** passing a **CookieCache** and a **CookiePersistor**:
```java
		ClearableCookieJar cookieJar = new PersistentCookieJar(
        new SetCookieCache(), new SharedPrefsCookiePersistor(context));
```

- Then just add the CookieJar when building your OkHttp client:
```java
		OkHttpClient okHttpClient = new OkHttpClient.Builder()
        		.cookieJar(cookieJar)
                .build();
```

## Installation instruction
```
- Add dependency in build.gradle file:
'Solution 1: local har package integration
1. Add the .har package to the lib folder.
2. Add the following code to the gradle of the entry:
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
implementation 'com.squareup.okhttp3:okhttp:3.10.0'

Solution 2: Add following dependencies in your build.gradle:
In project level build.gradle:

allprojects{
    repositories{
        mavenCentral()
    }
}

Add the following code to the entry gradle:

implementation 'io.openharmony.tpc.thirdlib:PersistentCookieJar:1.0.1'
```


## License
-------
    Copyright 2016 Francisco Jos?Montiel Navarro

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.