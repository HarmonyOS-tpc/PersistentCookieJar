/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.franmontiel.persistentcookiejar;

import okhttp3.Cookie;

/**
 * This class is used to test create cookies based on domain & HttpUrl
 */
class TestCookieCreator {
    private static final String DEFAULT_DOMAIN = "domain.com";
    private static final String DEFAULT_PATH = "/";

    private TestCookieCreator() {
    }

    /**
     * This method is used to create persistent cookie based on hostOnlyDomain flag
     *
     * @param hostOnlyDomain true/false which will be passed based on the test
     * @return Cookie object return by HttpUrl
     */
    public static Cookie createPersistentCookie(boolean hostOnlyDomain) {
        Cookie.Builder builder = new Cookie.Builder()
                .path(DEFAULT_PATH)
                .name("name")
                .value("value")
                .expiresAt(System.currentTimeMillis() + 24 * 60 * 60 * 1000)
                .httpOnly()
                .secure();
        if (hostOnlyDomain) {
            builder.hostOnlyDomain(DEFAULT_DOMAIN);
        } else {
            builder.domain(DEFAULT_DOMAIN);
        }
        return builder.build();
    }

    /**
     * This method is used to create persistent cookie based on name & value provided at the time of test
     *
     * @param name  String value to be send for the test
     * @param value String value to be send for the test
     * @return Cookie object return by HttpUrl based on name & value
     */
    public static Cookie createPersistentCookie(String name, String value) {
        return new Cookie.Builder()
                .domain(DEFAULT_DOMAIN)
                .path(DEFAULT_PATH)
                .name(name)
                .value(value)
                .expiresAt(System.currentTimeMillis() + 24 * 60 * 60 * 1000)
                .httpOnly()
                .secure()
                .build();
    }

    /**
     * This method is used to create non-persistent cookie
     *
     * @return Non-persistent Cookie object
     */
    public static Cookie createNonPersistentCookie() {
        return new Cookie.Builder()
                .domain(DEFAULT_DOMAIN)
                .path(DEFAULT_PATH)
                .name("name")
                .value("value")
                .httpOnly()
                .secure()
                .build();
    }

    /**
     * This method is used to create non-persistent cookie based on the name & value
     *
     * @param name  String value to be send for the test
     * @param value String value to be send for the test
     * @return Non-persistent Cookie object return by HttpUrl based on name & value
     */
    public static Cookie createNonPersistentCookie(String name, String value) {
        return new Cookie.Builder()
                .domain(DEFAULT_DOMAIN)
                .path(DEFAULT_PATH)
                .name(name)
                .value(value)
                .httpOnly()
                .secure()
                .build();
    }

    /**
     * This method is used to create expired cookie
     *
     * @return Expired Cookie object return by HttpUrl
     */
    public static Cookie createExpiredCookie() {
        return new Cookie.Builder()
                .domain(DEFAULT_DOMAIN)
                .path(DEFAULT_PATH)
                .name("name")
                .value("value")
                .expiresAt(Long.MIN_VALUE)
                .httpOnly()
                .secure()
                .build();
    }
}