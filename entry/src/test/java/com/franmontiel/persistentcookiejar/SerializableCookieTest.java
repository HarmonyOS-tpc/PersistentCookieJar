/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.franmontiel.persistentcookiejar;

import com.franmontiel.persistentcookiejar.persistence.SerializableCookie;

import okhttp3.Cookie;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * This class is used to run test case with SerializableCookie
 */
public class SerializableCookieTest {
    /**
     * This method is used to compare serialize & deserialize cookie
     */
    @Test
    public void cookieSerialization() {
        Cookie cookie = TestCookieCreator.createPersistentCookie(false);
        String serializedCookie = new SerializableCookie().encode(cookie);
        Cookie deserializedCookie = new SerializableCookie().decode(serializedCookie);
        assertEquals(cookie, deserializedCookie);
    }

    /**
     * This method is used to compare serialize & deserialize persistent cookie
     */
    @Test
    public void hostOnlyDomainCookieSerialization() {
        Cookie cookie = TestCookieCreator.createPersistentCookie(true);
        String serializedCookie = new SerializableCookie().encode(cookie);
        Cookie deserializedCookie = new SerializableCookie().decode(serializedCookie);
        assertEquals(cookie, deserializedCookie);
    }

    /**
     * This method is used to compare serialize & deserialize non-persistent cookie
     */
    @Test
    public void nonPersistentCookieSerialization() {
        Cookie cookie = TestCookieCreator.createNonPersistentCookie();
        String serializedCookie = new SerializableCookie().encode(cookie);
        Cookie deserializedCookie = new SerializableCookie().decode(serializedCookie);
        assertEquals(cookie, deserializedCookie);
    }
}